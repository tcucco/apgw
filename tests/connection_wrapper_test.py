# pylint: disable=missing-docstring
from contextlib import asynccontextmanager
from unittest import TestCase

from apgw import ConnectionArgs, ConnectionWrapper, DictConstraint

from tests.core import async_test


class ConnectionWrapperTest(TestCase):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.db = None

    async def async_set_up(self):
        pass

    async def async_tear_down(self):
        if self.db is not None and self.db.is_open:
            await self.db.close()

    def get_db(self, auto_open=True):
        if self.db is None:
            self.db = MockDB({}, auto_open=auto_open)
        return self.db

    @async_test
    async def test_is_open(self):
        db = self.get_db()
        self.assertFalse(db.is_open)
        await db.open()
        self.assertTrue(db.is_open)

    @async_test
    async def test_conn(self):
        db = self.get_db()
        with self.assertRaises(Exception):
            _ = db.connection
        await db.open()
        self.assertIsNotNone(db.connection)

    @async_test
    async def test_transaction(self):
        db = self.get_db()
        async with db.transaction():
            pass
        self.assertEqual(db.connection.transaction_count, 1)

    @async_test
    async def test_async_context_manager(self):
        db = self.get_db()
        self.assertFalse(db.is_open)
        async with db:
            self.assertTrue(db.is_open)
        self.assertFalse(db.is_open)

    @async_test
    async def test_fetch_auto_open_true(self):
        db = self.get_db(True)
        await db.select("users", DictConstraint([("id", 1)]))
        self.assertTrue(db.is_open)

    @async_test
    async def test_fetch_auto_open_false(self):
        db = self.get_db(auto_open=False)
        with self.assertRaises(Exception):
            await db.select("users", DictConstraint([("id", 1)]))

    @async_test
    async def test_fetchrow_auto_open_true(self):
        db = self.get_db()
        await db.select_one("users", DictConstraint([("id", 1)]))
        self.assertTrue(db.is_open)

    @async_test
    async def test_fetchrow_auto_open_false(self):
        db = self.get_db(auto_open=False)
        with self.assertRaises(Exception):
            await db.select_one("users", DictConstraint([("id", 1)]))

    @async_test
    async def test_select(self):
        db = self.get_db()
        await db.open()
        fetch_result = [{"id": 1}, {"id": 2}]
        db.connection.fetch_result = fetch_result
        result = await db.select("users", DictConstraint([("id", 1)]))
        self.assertEqual(result, fetch_result)

    @async_test
    async def test_select_and_count(self):
        db = self.get_db()
        await db.open()
        fetch_result = [{"id": 1}, {"id": 2}]
        fetchrow_result = 2
        db.connection.fetch_result = fetch_result
        db.connection.fetchrow_result = {"c": fetchrow_result}
        (records, count) = await db.select_and_count("users")
        self.assertEqual(fetch_result, records)
        self.assertEqual(fetchrow_result, count)

    @async_test
    async def test_select_one(self):
        db = self.get_db()
        await db.open()
        fetchrow_result = {"id": 1}
        db.connection.fetchrow_result = fetchrow_result
        result = await db.select_one("users")
        self.assertEqual(fetchrow_result, result)

    @async_test
    async def test_exists_true(self):
        db = self.get_db()
        await db.open()
        fetchrow_result = [{"id": 1}]
        db.connection.fetchrow_result = fetchrow_result
        result = await db.exists("users")
        self.assertTrue(result)

    @async_test
    async def test_insert(self):
        db = self.get_db()
        await db.open()
        data = {"id": 1, "name": "John Smith"}
        db.connection.fetchrow_result = data
        result = await db.insert("users", data)
        self.assertEqual(data, result)

    @async_test
    async def test_create(self):
        db = self.get_db()
        await db.open()
        data = {"id": 1, "name": "John Smith"}
        db.connection.fetchrow_result = data
        result = await db.create("users", data)
        self.assertEqual(data, result)

    @async_test
    async def test_update(self):
        db = self.get_db()
        await db.open()
        updates = DictConstraint({"name": "John Smith"})
        constraints = DictConstraint({"id": 1})
        data = [{**updates, **constraints}]
        db.connection.fetch_result = data
        result = await db.update("users", updates, constraints, returning="*")
        self.assertEqual(data, result)

    @async_test
    async def test_delete(self):
        db = self.get_db()
        await db.open()
        data = [
            {"id": 1, "name": "John Smith"},
            {"id": 2, "name": "Jane Smith"},
        ]
        db.connection.fetch_result = data
        result = await db.delete("users", returning="*")
        self.assertEqual(result, data)


class MockDB(ConnectionWrapper):
    def __init__(self, conn_args: ConnectionArgs, *, auto_open: bool):
        self._conn_args = conn_args
        self._should_auto_open = auto_open
        super().__init__()

    async def get_connection_args(self) -> ConnectionArgs:
        return self._conn_args

    @property
    def should_auto_open(self):
        return self._should_auto_open

    async def connect(self, connection_args: ConnectionArgs):
        # pylint: disable=no-self-use
        return ConnectionStub(**connection_args)


class ConnectionStub:
    """Stub the simulates a database connection."""

    # pylint: disable=too-many-instance-attributes

    def __init__(self, *args, **kwargs):
        self.args = args
        self.kwargs = kwargs
        self.transaction_count = 0
        self.fetch_result = None
        self.fetchrow_result = None
        self.last_sql = None
        self.last_params = None
        self._in_trans = False

    async def close(self):
        pass

    @asynccontextmanager
    async def transaction(self):
        if self._in_trans:
            raise Exception("Cannot have two open transactions at once")
        self.transaction_count += 1
        self._in_trans = True
        try:
            yield self
        finally:
            self._in_trans = False

    async def fetch(self, sql, *params):
        self.last_sql = sql
        self.last_params = params
        return self.fetch_result or []

    async def fetchrow(self, sql, *params):
        self.last_sql = sql
        self.last_params = params
        return self.fetchrow_result
