# pylint: disable=missing-docstring
from unittest import TestCase

from apgw import builder
from apgw.constraint import (BinaryConstraint, DictConstraint, GroupConstraint,
                             Literal, MappedPlaceholder, TextConstraint)


class BuilderTest(TestCase):
    def test_constraints_to_sql_none(self):
        self.assertEqual(builder.constraints_to_sql(None), (None, []))

    def test_constraints_to_sql_text_constraint(self):
        self.assertEqual(
            builder.constraints_to_sql(TextConstraint("name = $0", ["trey"])),
            ("name = $1", ["trey"]),
        )

    def test_constraints_to_sql_dict_constraint(self):
        self.assertEqual(
            builder.constraints_to_sql(DictConstraint([("name", "trey"), ("age", 33)])),
            ("(name = $1) and (age = $2)", ["trey", 33]),
        )

    def test_constraints_to_sql_list_of_constraint(self):
        self.assertEqual(
            builder.constraints_to_sql(
                [
                    BinaryConstraint("name", "trey"),
                    BinaryConstraint("username", None, "!="),
                    BinaryConstraint("age", 33, "<="),
                ]
            ),
            ("(name = $1) and (username is not null) and (age <= $2)", ["trey", 33]),
        )

    def test_constraints_to_sql_in_operator(self):
        self.assertEqual(
            builder.constraints_to_sql(
                [
                    BinaryConstraint("id", [10, 11, 12], "in"),
                    BinaryConstraint("name", "Trey"),
                    BinaryConstraint("age", [30, 31, 32], "in"),
                ]
            ),
            (
                "(id in ($1, $2, $3)) and (name = $4) and (age in ($5, $6, $7))",
                [10, 11, 12, "Trey", 30, 31, 32],
            ),
        )

    def test_constraints_to_sql_not_in_operator(self):
        self.assertEqual(
            builder.constraints_to_sql(
                [
                    BinaryConstraint("id", [10, 11, 12], "in"),
                    BinaryConstraint("name", "Trey"),
                    BinaryConstraint("age", [30, 31, 32], "not in"),
                ]
            ),
            (
                "(id in ($1, $2, $3)) and (name = $4) and (age not in ($5, $6, $7))",
                [10, 11, 12, "Trey", 30, 31, 32],
            ),
        )

    def test_constraints_to_sql_single_constraint(self):
        self.assertEqual(
            builder.constraints_to_sql(BinaryConstraint("name", "trey")),
            ("(name = $1)", ["trey"]),
        )

    def test_constraints_to_sql_group(self):
        constraints = GroupConstraint(
            "or",
            [
                BinaryConstraint("id", 10, "<"),
                BinaryConstraint("id", 20, ">"),
            ],
        )
        (sql, params) = builder.constraints_to_sql(constraints)
        self.assertEqual(sql, "(id < $1) or (id > $2)")
        self.assertEqual(params, [10, 20])

    def test_constraints_to_sql_nested_groups(self):
        constraints = GroupConstraint(
            "and",
            [
                GroupConstraint(
                    "or",
                    [
                        BinaryConstraint("first_name", "Trey", "="),
                        BinaryConstraint("first_name", "Frank", "="),
                    ],
                ),
                GroupConstraint(
                    "or",
                    [
                        BinaryConstraint("age", 30, "<"),
                        BinaryConstraint("age", 40, ">="),
                    ],
                ),
            ],
        )
        (sql, params) = builder.constraints_to_sql(constraints)
        self.assertEqual(
            sql,
            "((first_name = $1) or (first_name = $2)) and ((age < $3) or (age >= $4))",
        )
        self.assertEqual(params, ["Trey", "Frank", 30, 40])

    def test_constraints_to_sql_mixed_binary_constraints_and_groups(self):
        constraints = GroupConstraint(
            "and",
            [
                BinaryConstraint("first_name", "Trey", "="),
                GroupConstraint(
                    "or",
                    [
                        BinaryConstraint("age", 30, "<"),
                        BinaryConstraint("age", 40, ">="),
                    ],
                ),
                BinaryConstraint("last_name", ["Cucco", "Smith"], "in"),
            ],
        )
        (sql, params) = builder.constraints_to_sql(constraints)
        self.assertEqual(
            sql,
            "(first_name = $1) and ((age < $2) or (age >= $3)) and (last_name in ($4, $5))",
        )
        self.assertEqual(params, ["Trey", 30, 40, "Cucco", "Smith"])

    def test_diff(self):
        source1 = {"name": "Trey", "age": 33, "height": "5'10"}
        updates1 = {"name": "Frank", "age": 33, "height": "5'11"}

        self.assertEqual(builder.diff(source1, source1), {})
        self.assertEqual(
            builder.diff(source1, updates1), {"name": "Frank", "height": "5'11"}
        )
        self.assertEqual(
            builder.diff(source1, updates1, {"name", "age"}), {"name": "Frank"}
        )

        with self.assertRaises(Exception):
            builder.diff({"a": 1}, {"b": 2})

        with self.assertRaises(Exception):
            builder.diff({"b": 1}, {"b": 2}, {"a"})

    def test_build_constraint(self):
        self.assertEqual(
            builder.build_constraint(
                {"id": 1, "name": "Trey", "age": 33, "occupation": "Software"},
                ["id", "name"],
            ),
            DictConstraint([("id", 1), ("name", "Trey")]),
        )

    def test_select_sql(self):
        self.assertEqual(builder.select_sql("users"), ("select * from users", []))

        self.assertEqual(
            builder.select_sql("users", [BinaryConstraint("name", "trey")]),
            ("select * from users where (name = $1)", ["trey"]),
        )

        self.assertEqual(
            builder.select_sql(
                "users", [BinaryConstraint("name", "trey")], columns="id, email"
            ),
            ("select id, email from users where (name = $1)", ["trey"]),
        )

        self.assertEqual(
            builder.select_sql(
                "users",
                [BinaryConstraint("name", "trey")],
                columns="id, email",
                order_by="email",
            ),
            ("select id, email from users where (name = $1) order by email", ["trey"]),
        )

        self.assertEqual(
            builder.select_sql(
                "users",
                [BinaryConstraint("name", "trey")],
                columns="id, email",
                order_by="email",
                limit=(5, 10),
            ),
            (
                "select id, email from users where (name = $1) order by email limit 5 offset 10",
                ["trey"],
            ),
        )

    def test_insert_sql(self):
        self.assertEqual(
            builder.insert_sql(
                "users",
                [("email", "fcucco@gmail.com"), ("name", "trey")],
                returning="id",
            ),
            (
                "insert into users (email, name) values ($1, $2) returning id",
                ["fcucco@gmail.com", "trey"],
            ),
        )

        self.assertEqual(
            builder.insert_sql(
                "users",
                DictConstraint([("email", "fcucco@gmail.com"), ("name", "trey")]),
            ),
            (
                "insert into users (email, name) values ($1, $2)",
                ["fcucco@gmail.com", "trey"],
            ),
        )

        self.assertEqual(
            builder.insert_sql(
                "users",
                DictConstraint(
                    [
                        ("id", Literal("uuid_generate_v4()")),
                        ("email", "fcucco@gmail.com"),
                        ("name", "trey"),
                        (
                            "age",
                            MappedPlaceholder(38, "coalesce({}, 40)".format),
                        ),
                    ]
                ),
            ),
            (
                (
                    "insert into users (id, email, name, age) "
                    "values (uuid_generate_v4(), $1, $2, coalesce($3, 40))"
                ),
                ["fcucco@gmail.com", "trey", 38],
            ),
        )

    def test_insert_many_sql(self):
        self.assertEqual(
            builder.insert_many_sql(
                "users",
                [
                    [("email", "fcucco@gmail.com"), ("name", "trey")],
                    [("email", "fcucco3@gmail.com"), ("name", "frank")],
                ],
                returning="id",
            ),
            (
                "insert into users (email, name) values ($1, $2) returning id",
                [["fcucco@gmail.com", "trey"], ["fcucco3@gmail.com", "frank"]],
            ),
        )

    def test_update_sql(self):
        self.assertEqual(
            builder.update_sql(
                "users",
                [
                    BinaryConstraint("name", "trey"),
                    BinaryConstraint(
                        "age",
                        MappedPlaceholder(33, "greatest(age, {})".format),
                    ),
                ],
            ),
            (
                "update users set name = $1, age = greatest(age, $2), updated_at = now()",
                ["trey", 33],
            ),
        )

        self.assertEqual(
            builder.update_sql(
                "users",
                [BinaryConstraint("name", "trey"), BinaryConstraint("age", 33)],
                timestamp_col=None,
            ),
            ("update users set name = $1, age = $2", ["trey", 33]),
        )

        self.assertEqual(
            builder.update_sql(
                "users",
                [BinaryConstraint("name", "trey"), BinaryConstraint("age", 33)],
                [BinaryConstraint("email", "gmail.com$", "~")],
                timestamp_col="updated_date",
                returning="*",
            ),
            (
                "update users "
                "set name = $1, age = $2, updated_date = now() "
                "where (email ~ $3) returning *",
                ["trey", 33, "gmail.com$"],
            ),
        )

        self.assertEqual(
            builder.update_sql(
                "users",
                [],
                [],
                timestamp_col="updated_date",
                returning="*",
            ),
            (
                "update users set updated_date = now() returning *",
                [],
            ),
        )

    def test_delete_sql(self):
        self.assertEqual(
            builder.delete_sql("users", returning="*"),
            ("delete from users returning *", []),
        )

        self.assertEqual(
            builder.delete_sql("users", [BinaryConstraint("age", None)]),
            ("delete from users where (age is null)", []),
        )
