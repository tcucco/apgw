"""Core test utils."""
import asyncio
from functools import wraps

_LOOP = asyncio.get_event_loop()


def async_test(fn):
    """This is an async decorator that makes an async test a synchronous test."""

    @wraps(fn)
    def wrapper(owner):
        """The wrapper function."""

        async def run():
            """Runs the wrapped method in a loop."""
            await owner.async_set_up()
            await fn(owner)
            await owner.async_tear_down()

        _LOOP.run_until_complete(run())

    return wrapper
