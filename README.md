# A simple wrapper around asyncpg

apgw is a simple wrapper around the excellent asyncpg library. It is designed to make building
certain types of simple queries very easy.

Please note: documentation on this project is currently not provided

## Interacting With the Project

`make` is the best way to interact with the project.

### Setting Up:

    make install

### Running Tests:

    make test

### Running Coverage

    make coverage
