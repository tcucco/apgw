"""Exceptions"""


class RollbackTransactionException(Exception):
    """Special exception to trigger a rollback."""
